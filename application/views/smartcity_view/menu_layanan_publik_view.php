<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>5AM LAYANAN PUBLIK</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <!--  -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
  
     <link rel="stylesheet" href="<?= base_url(); ?>assets/css/tripo-icons.css">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>
</head>

<body>
    <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list banner-style-one">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">LAYANAN PUBLIK KOTA MALANG</h1><br><br>
        </center>

<h3  style=" font-family: Arial, Helvetica, sans-serif;"></h3><br>
 <div class="row">
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://sambat.malangkota.go.id/" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/sambat.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>SAMBAT ONLINE</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://cctv.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/cctv.png"  style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>CCTV<br> </a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->


                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://112.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/ngalam112.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-phone-call"></i> -->
                            <h3>NGALAM 112</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <!-- baris kedua -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://mediacenter.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/media_center.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>MEDIA CENTER</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://siapel.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/siapel.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>SIAPEL</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://pajak.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/pajak.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>PAJAK</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="100ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://izol.malangkota.go.id/index.php/account/login" target="_blank">
                           <!--  <i class="tripo-icon-deer"></i> -->
                           <img src="<?= base_url(); ?>assets/images/icon_new/izol.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>IZIN ONLINE <br> </a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="http://sembako.malangkota.go.id" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/harga_pangan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>HARGA PANGAN</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="200ms">
                    <div class="tour-category-one__single rounded-lg">
                           <!--  <i class="tripo-icon-sunset"></i> -->
                           <a href="https://covid19.malangkota.go.id/" target="_blank">
                           <img src="<?= base_url(); ?>assets/images/icon_new/covid.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3><a href="tour-standard.html">COVID19<br></a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="http://ruvid.ub.ac.id/" target="_blank">
                            <img src="<?= base_url(); ?>assets/images/icon_new/ruvid.png" style="height: 100px; width: 100px" class="main-logo"  width="" alt="Awesome Image" />
                        <!-- <i class="tripo-icon-message"></i> -->
                        <h3>RUVID<br>RUJUKAN COVID</a></h3>
                    </div><!-- /.tour-category-one__single -->
                    </div>

                       <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="https://www.instagram.com/pasarrakyatmalang/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/pasar_rakyat.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>PASAR RAKYAT</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->
                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="https://play.google.com/store/apps/details?id=com.rochmadadhimgmail.sampade" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/sampade.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>SAMPADE</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://sibansos.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/sibansos.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-paragliding"></i> -->
                            <h3>SIBANSOS<br></a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://si-petarungv2.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/sipetarung.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>SI PETARUNG V2</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="https://carijagoan.id/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/cari_jagoan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>CARI JAGOAN</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="https://pasarmbois.com/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/pasar_mbois.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>PASAR MBOIS</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->
                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://play.google.com/store/apps/details?id=id.co.adythree.simonparma" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/simonparma.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>SITOKIRMA</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://play.google.com/store/apps/details?id=id.co.adythree.sisparma" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/sisparma.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>SISPARMA</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://lapangandisporapar.id/" target="_blank">
                            <img src="<?= base_url(); ?>assets/images/icon_new/simbahe.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                        <!-- <i class="tripo-icon-message"></i> -->
                        <h3>SIMBAH-E <br></a></h3>
                    </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://vaksinlansia.malangkota.go.id/" target="_blank">
                            <img src="<?= base_url(); ?>assets/images/icon_new/vaskinlansia.png" style="height: 100px; width: 100px" class="main-logo"  width="" alt="Awesome Image" />
                        <!-- <i class="tripo-icon-message"></i> -->
                        <h3>DAFTAR <br>VAKSIN LANSIA</a></h3>
                    </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://ujikir.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/ujikir.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>UJI KIR</a></h3>
                     </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->
                    
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="http://si-paldi.malangkota.go.id/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/sipaldi.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3> PEMANTAUAN <br> AIR LIMBAH</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="http://www.pdamkotamalang.com/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/pdam.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>PDAM<BR>KOTA MALANG</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                                <div class="tour-category-one__single rounded-lg">
                                    <a href="https://kipop.malangkota.go.id/" target="_blank">
                                        <img src="<?= base_url(); ?>assets/images/icon_new/kipop.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                                    <!-- <i class="tripo-icon-message"></i> -->
                                    <h3>KIPOP<BR>KOTA MALANG</a></h3>
                                </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->


</div> <!-- tutup row  bagian dan kantor-->



 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
       <footer class="site-footer">
        <img src="<?= base_url(); ?>assets/images/wave3.png" style="width: 100%" class="banner-satu" width="" alt="Awesome Image" />
                      <!-- /.site-footer__bg -->
        <!-- /.site-footer__bg -->

                <div class="site-footer__bottom">
                      <div class="container">
                          <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
                          Pemerintah Kota Malang</a></p>
                          <div class="site-footer__social">
                            <a href="https://www.facebook.com/pemkot.malang/"><i class="fa fa-facebook-square"></i></a>
                            <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a>
                          </div><!-- /.site-footer__social -->
                      </div><!-- /.container -->
                  </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

 <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/typed-2.0.11.js"></script>
    <script src="<?= base_url(); ?>assets/js/vegas.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="<?= base_url(); ?>assets/js/theme.js"></script>
</body>

</html>
