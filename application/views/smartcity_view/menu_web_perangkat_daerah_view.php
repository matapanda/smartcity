<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Perangkat Daerah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   

    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
    <link rel="manifest" href="<?= base_url(); ?>assets/images/favicons/site.webmanifest">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>
</head>

<body>
   <body>
   <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list banner-style-one">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">SMART GOVERNANCE</h1><br><br>
        </center>
<!-- bagian dan kantor -->
<h3  style=" font-family: Arial, Helvetica, sans-serif;">BADAN DAN KANTOR</h3><br>
 <div class="row">
     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://inspektoratdaerah.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/inspektorat.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>INSPEKTORAT</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://bappeda.malangkota.go.id/web/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bappeda.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAPPEDA <!-- BADAN PERENCANAAN PEMBANGUNAN DAERAH --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://bapenda.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bapenda.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAPENDA<!-- BADAN PENDAPATAN DAERAH --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://bkpsdm.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bkpsdm.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BKPSDM<!-- BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://bakesbangpol.malangkota.go.id/author/bakesbangpol/page/19/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bakesbangpol.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAKESBANGPOL<!-- BADAN KESATUAN BANGSA DAN POLITIK --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://bkad.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bkad.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BKAD<!-- BADAN KEUANGAN DAN ASET DAERAH --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

        <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://bpbd.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/bpbd.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BPBD<!-- BADAN PENANGGULANGAN BENCANA DAERAH --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://satpolpp.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/badan/satpolpp.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SATPOLPP<!-- SATUAN POLISI PAMONG PRAJA --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

</div> <!-- tutup row  bagian dan kantor-->

<!-- bagian sekretariat -->
<h3  style=" font-family: Arial, Helvetica, sans-serif;">BAGIAN SEKRETARIAT DAERAH KOTA MALANG</h3><br>
 <div class="row">
     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://humas.malangkota.go.id/page/47/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_humas.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN<BR>HUMAS</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href=" http://kesra.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_kesra.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN<BR>KESRA <!-- BAGIAN KESEJAHTERAAN RAKYAT DAN KEMASYARAKATAN --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://hukum.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_hukum.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN<BR>HUKUM</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://pisda.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_perekonomian.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN <BR>PEREKONOMIAN<!-- BAGIAN PEREKONOMIAN, INFRASTRUKTUR dan SDA --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://umum.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_umum.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN <BR>UMUM</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://organisasi.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_organisasi.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN<BR>ORGANISASI</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

        <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://blp.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/blp.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BLP<BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://pemerintahan.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bagian/bag_pemerintahan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAGIAN<BR>PEMERINTAHAN<!-- SATUAN POLISI PAMONG PRAJA --></a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

</div> <!-- tutup row -->



         <h3  style=" font-family: Arial, Helvetica, sans-serif;">DINAS</h3><br>
   <div class="row">
             <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://disnakerpmptsp.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/disnaker.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>DISNAKERPMPTSP</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
            <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="100ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://kominfo.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/diskominfo.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-list-menu"></i> -->
                    <h3>DINAS KOMINFO</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
            <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="200ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dikbud.malangkota.go.id/"  target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinas_pendidikan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-sunset"></i> -->
                    <h3>DINAS PENDIDIKAN</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
            <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dinkes.malangkota.go.id"  target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinkes.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-paragliding"></i> -->
                    <h3>DINAS KESEHATAN</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
            <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dishub.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dishub.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DINAS PERHUBUNGAN</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dispangtan.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinas_ketahanpangan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DISPANGTAN</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dpuprpkp.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dpupr.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DPUPRPERKIM</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://sosdp3ap2kb.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinsos.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DINSOSP3AP2KB</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://diskopindag.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinas_koperasi.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DISKOPERINDAG</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://disporapar.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinas_pariwisata.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DISPORA dan PARIWISATA</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dispendukcapil.malangkota.go.id" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dispenduk.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DISPENDUKCAPIL</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://dlh.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dlh.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DINAS LINGKUNGAN HIDUP</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->
              <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://dispussipda.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dinas/dinas_perpustakaan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                   <!--  <i class="tripo-icon-phone-call"></i> -->
                    <h3>DISPUSSIPDA</a></h3>
                </div><!-- /.tour-category-one__single -->
            </div><!-- /.tour-category-one__col -->

        </div><!-- /.row -->

        <!-- kec kel -->
<h3  style=" font-family: Arial, Helvetica, sans-serif;">KECAMATAN</h3><br>
 <div class="row">
     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/menu_kec_klojen" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kec/klojen.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KECAMATAN<br> KLOJEN</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/menu_kec_blimbing/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kec/blimbing.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KECAMATAN<br> BLIMBING</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/menu_kec_lowokwaru/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kec/lowokwaru.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KECAMATAN<br> LOWOKWARU</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/menu_kec_sukun/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kec/sukun.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KECAMATAN<br> SUKUN</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/menu_kec_kedungkandang" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kec/kedungkandang.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KECAMATAN<br> KEDUNGKANDANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

    </div> <!-- tutup row -->

 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
      <img src="<?= base_url(); ?>assets/images/iconic_mlg_nonbg.png" alt="Awesome Image" />

    <!-- /.site-footer__bg -->

    <div class="site-footer__bottom">
        <div class="container">
            <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
            Pemerintah Kota Malang</a></p>
            <div class="site-footer__social">
              <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i></a>
              <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i></a>
            </div><!-- /.site-footer__social -->
        </div><!-- /.container -->
    </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

  <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>

</html>
