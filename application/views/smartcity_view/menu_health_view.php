<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5AM HEALTH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   

    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
    <link rel="manifest" href="<?= base_url(); ?>assets/images/favicons/site.webmanifest">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>
</head>

<body>
    <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

          <!--   <img src="<?= base_url(); ?>assets6/images/balaikota_tugu.png" class="banner-mock" alt="Awesome Image" /> -->
 <section class="tour-two tour-list banner-style-one">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">SMART HEALTH</h1><br><br>
        </center>

<h3  style=" font-family: Arial, Helvetica, sans-serif;"></h3><br>
 <div class="row">
    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="200ms">
    <div class="tour-category-one__single rounded-lg">
           <!--  <i class="tripo-icon-sunset"></i> -->
           <a href="https://covid19.malangkota.go.id/" target="_blank">
           <img src="<?= base_url(); ?>assets/images/icon_new/covid.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
            <h3><a href="tour-standard.html">WEBSITE <br> COVID19<br></a></h3>
        </div><!-- /.tour-category-one__single -->
    </div><!-- /.tour-category-one__col -->
    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
    <div class="tour-category-one__single rounded-lg">
        <a href="http://ruvid.ub.ac.id/" target="_blank">
            <img src="<?= base_url(); ?>assets/images/icon_new/ruvid.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
        <!-- <i class="tripo-icon-message"></i> -->
        <h3>RUVID<br>RUJUKAN COVID</a></h3>
    </div><!-- /.tour-category-one__single -->
</div>
    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
    <div class="tour-category-one__single rounded-lg">
        <a href="https://vaksinlansia.malangkota.go.id/" target="_blank">
            <img src="<?= base_url(); ?>assets/images/icon_new/vaskinlansia.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
        <!-- <i class="tripo-icon-message"></i> -->
        <h3>DAFTAR <br>VAKSIN LANSIA</a></h3>
    </div><!-- /.tour-category-one__single -->
    </div><!-- /.tour-category-one__col -->

</div> <!-- tutup row  bagian dan kantor-->



 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
      <img src="<?= base_url(); ?>assets/images/iconic_mlg_nonbg.png" alt="Awesome Image" />

    <!-- /.site-footer__bg -->

    <div class="site-footer__bottom">
        <div class="container">
            <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
            Pemerintah Kota Malang</a></p>
            <div class="site-footer__social">
              <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i></a>
              <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i></a>
            </div><!-- /.site-footer__social -->
        </div><!-- /.container -->
    </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

 <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/typed-2.0.11.js"></script>
    <script src="<?= base_url(); ?>assets/js/vegas.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="<?= base_url(); ?>assets/js/theme.js"></script>
</body>

</html>
