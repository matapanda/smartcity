<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>5AM SMART CITY</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <!--  -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
  


    <style type="text/css">
        .no-gutter > [class*='col-'] {
        padding-right:0;
        padding-left:0;
        }

        .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
        }

       

        .button1:hover {
        color: #fff;
        background-color: #E94FCA;
        border-color: #E94FCA; }


        .button2 {
        background-color: #676F95;
        color: white;
        border: 2px solid #676F95;
        }

        .button2:hover {
        background-color: #555555;
        color: white;
        }

        .button3 {
        background-color: #7D86B2;
        color: white;
        border: 2px solid #7D86B2;
        }

        .button3:hover {
        background-color: #555555;
        color: white;
        }

        .button4 {
        background-color: #96A0D2;
        color: white;
        border: 2px solid #96A0D2;
        }

        .button4:hover {background-color: #555555;}

        .button5 {
        background-color: #B7C1F0;
        color: white;
        border: 2px solid #B7C1F0;
        }

        .button5:hover {
        background-color: #555555;
        color: white;
        }
        .button6 {
        background-color: #bcc4e6;
        color: white;
        border: 2px solid #bcc4e6;
        }

        .button6:hover {
        background-color: #555555;
        color: white;
        }
        .button7 {
        background-color: #0099CC;
        color: white;
        border: 2px solid #0099CC;
        }

        .button7:hover {
        background-color: #555555;
        color: white;
        }

        .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
        }
          </style>

        
           <style>
          img {
              width: 100%;
              height: auto;
          }
          </style>
</head>

<body>
    <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

          <!--   <img src="<?= base_url(); ?>assets6/images/balaikota_tugu.png" class="banner-mock" alt="Awesome Image" /> -->
 <section class="tour-two tour-list banner-style-one">
    <div class="container">
       <img src="<?= base_url(); ?>assets/images/headernew.png" width="" alt="Awesome Image" />

        <div class="col-xl-6 col-lg-8">
                        <div class="content-block">
                          
                            <div class="button-block">
                               <a href="<?= base_url(); ?>smartcity/tentang" class="banner-btn">
                                    <i class="fa fa-info"></i>
                                    Tentang<span>Kota Malang Smartcity
                                </a>
                               
                            </div><!-- /.button-block -->
                        </div><!-- /.content-block -->
                    </div><!-- /.col-lg-6 -->
        <div class="block-title text-center">

           
        </div><!-- /.block-title -->
      
        <div class="row">

                    <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="100ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_economy" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/smart_economy.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-list-menu"></i> -->
                            <h3>Smart Economy</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_governance" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/smart_goverment.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Smart Governance</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_branding" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/smart_brending.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Smart Branding</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_living" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/smart_living.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Smart Living</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_society" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/smart_society.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Smart Society</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/smart_environment" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/smart_environment.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Smart Environment</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                      <div class="tour-category-one__single rounded-lg">
                              <a href="<?= base_url(); ?>smartcity/layananpublik"> 
                              <img src="<?= base_url(); ?>assets/images/icon_new/layanan_publik.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                          <!-- <i class="tripo-icon-message"></i> -->
                          <h3>Layanan Publik</a></h3>
                      </div><!-- /.tour-category-one__single -->
                   </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://sambat.malangkota.go.id/" target="_blank">
                              <img src="<?= base_url(); ?>assets/images/icon_new/sambat.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-message"></i> -->
                            <h3>Sambat</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="200ms">
                        <div class="tour-category-one__single rounded-lg" >
                            <a href="https://sembako.malangkota.go.id" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/harga_pangan.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-sunset"></i> -->
                            <h3>Harga Pangan</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://cctv.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/cctv.png"  style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>CCTV<br> </a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                   

                      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://112.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/ngalam112.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-phone-call"></i> -->
                            <h3>Ngalam 112</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <!-- baris kedua -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://mediacenter.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/media_center.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>Media Center</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://siapel.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/siapel.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>SIAPEL</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://pajak.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-flag"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/pajak.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>Pajak</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                    <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="100ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://izol.malangkota.go.id/index.php/account/login" target="_blank">
                           <!--  <i class="tripo-icon-deer"></i> -->
                           <img src="<?= base_url(); ?>assets/images/icon_new/izol.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>Izin Online <br> </a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://rsud.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/rsud.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-paragliding"></i> -->
                            <h3>RSUD</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                      <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="<?= base_url(); ?>smartcity/menu_puskesmas" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/puskesmas.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-paragliding"></i> -->
                            <h3>PUSKESMAS</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->



                     <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="http://pdam.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/pdam.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-paragliding"></i> -->
                            <h3>PDAM</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://sibansos.malangkota.go.id/" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/sibansos.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <!-- <i class="tripo-icon-paragliding"></i> -->
                            <h3>Sibansos<br></a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                   <div class="tour-category-one__col wow fadeInDown " data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://www.bmkg.go.id/cuaca/prakiraan-cuaca.bmkg?Kota=Kota%20Malang&AreaID=501290&Prov=12" target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/cuaca.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                           <!--  <i class="tripo-icon-paragliding"></i> -->
                            <h3>Cuaca</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                        <div class="tour-category-one__single rounded-lg">
                            <a href="https://si-petarungv2.malangkota.go.id/" target="_blank">
                            <!-- <i class="tripo-icon-message"></i> -->
                            <img src="<?= base_url(); ?>assets/images/icon_new/sipetarung.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                            <h3>Si Petarung V2</a></h3>
                        </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->
                  <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://pkk.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/pkk.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>PKK</a></h3>
                     </div><!-- /.tour-category-one__single -->
                  </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://jdih.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/jdih.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>JDIH</a></h3>
                     </div><!-- /.tour-category-one__single -->
                  </div><!-- /.tour-category-one__col -->

                   <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://aikid.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/aikid.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>AIKID</a></h3>
                     </div><!-- /.tour-category-one__single -->
                  </div><!-- /.tour-category-one__col -->

                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://rph.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/rph.png" style="height: 110px; width: 110px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>RPH</a></h3>
                     </div><!-- /.tour-category-one__single -->
                  </div><!-- /.tour-category-one__col -->
                  
                </div><!-- /.row -->
    </div><!-- /.container -->

</section><!-- /.gallery-one -->


    <footer class="site-footer">
        <img src="<?= base_url(); ?>assets/images/wave3.png" style="width: 100%" class="banner-satu" width="" alt="Awesome Image" />
                      <!-- /.site-footer__bg -->
        <!-- /.site-footer__bg -->

                <div class="site-footer__bottom">
                      <div class="container">
                          <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
                          Pemerintah Kota Malang</a></p>
                          <div class="site-footer__social">
                            <a href="https://www.facebook.com/malangkota.go.id"><i class="fa fa-facebook-square"></i></a>
                            <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a>
                          </div><!-- /.site-footer__social -->
                      </div><!-- /.container -->
                  </div><!-- /.site-footer__bottom -->


</footer>
</div>
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">      <a href="<?= base_url(); ?>smartcity" class="main-nav__logo">
                      <img src="<?= base_url(); ?>assets/images/logo_navbar.png" class="main-logo" width="123" alt="Awesome Image" />
                  </a>

                      <!-- /.smpl-icon-menu --></a>
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep">



            </div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/malangkota.go.id"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

    <!-- /.scroll-to-top -->
    <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>

</body>

</html>
