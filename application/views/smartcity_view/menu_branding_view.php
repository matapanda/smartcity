<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5AM Smart Branding</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   

    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
    <link rel="manifest" href="<?= base_url(); ?>assets/images/favicons/site.webmanifest">
     <link rel="stylesheet" href="<?= base_url(); ?>assets/css/tripo-icons.css">

    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>
</head>

<body>
   <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list banner-style-one">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">SMART BRANDING</h1><br><br>
        </center>

<h3  style=" font-family: Arial, Helvetica, sans-serif;"></h3><br>
 <div class="row">
     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://play.google.com/store/apps/details?id=com.lunartech.lovelymalang&hl=in&gl=US" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/smart_brending.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>MALANG MENYAPA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
       <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://festivalmbois.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/mbois.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>FESTIVAL MBOIS</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://osiker.com/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/osiker.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>OSIKER</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->


</div> <!-- tutup row  bagian dan kantor-->



 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
    
    <!-- /.site-footer__bg -->

   <footer class="site-footer">
        <img src="<?= base_url(); ?>assets/images/wave3.png" style="width: 100%" class="banner-satu" width="" alt="Awesome Image" />
                      <!-- /.site-footer__bg -->
        <!-- /.site-footer__bg -->

                <div class="site-footer__bottom">
                      <div class="container">
                          <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
                          Pemerintah Kota Malang</a></p>
                          <div class="site-footer__social">
                            <a href="https://www.facebook.com/malangkota.go.id"><i class="fa fa-facebook-square"></i></a>
                            <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a>
                          </div><!-- /.site-footer__social -->
                      </div><!-- /.container -->
                  </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
           <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/malangkota.go.id"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

   <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>

</html>
