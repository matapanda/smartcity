<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5AM Smart Governance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   

    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
    <link rel="manifest" href="<?= base_url(); ?>assets/images/favicons/site.webmanifest">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>
    <style type="text/css">
        .no-gutter > [class*='col-'] {
        padding-right:0;
        padding-left:0;
        }

        .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
        }

       

        .button1:hover {
        color: #fff;
        background-color: #E94FCA;
        border-color: #E94FCA; }


        .button2 {
        background-color: #676F95;
        color: white;
        border: 2px solid #676F95;
        }

        .button2:hover {
        background-color: #555555;
        color: white;
        }

        .button3 {
        background-color: #7D86B2;
        color: white;
        border: 2px solid #7D86B2;
        }

        .button3:hover {
        background-color: #555555;
        color: white;
        }

        .button4 {
        background-color: #96A0D2;
        color: white;
        border: 2px solid #96A0D2;
        }

        .button4:hover {background-color: #555555;}

        .button5 {
        background-color: #B7C1F0;
        color: white;
        border: 2px solid #B7C1F0;
        }

        .button5:hover {
        background-color: #555555;
        color: white;
        }
        .button6 {
        background-color: #bcc4e6;
        color: white;
        border: 2px solid #bcc4e6;
        }

        .button6:hover {
        background-color: #555555;
        color: white;
        }
        .button7 {
        background-color: #0099CC;
        color: white;
        border: 2px solid #0099CC;
        }

        .button7:hover {
        background-color: #555555;
        color: white;
        }

        .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
        }
          </style>
</head>

<body>
  <div class="preloader">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="https://malangkota.go.id/">
                            <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                        </a>
                      
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                      <li style="color: transparent;"></li>
                    </div><!-- /.navbar-collapse -->
                   
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list banner-style-one">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">SMART GOVERNANCE</h1><br><br>
        </center>
<!-- bagian dan kantor -->
<h3  style=" font-family: Arial, Helvetica, sans-serif;">INSTANSI</h3><br>
 <div class="row">
     <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="<?= base_url(); ?>smartcity/webperangkatdaerah"> 
                        <img src="<?= base_url(); ?>assets/images/icon_new/web_perangkat_daerah.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>PERANGKAT <BR> DAERAH</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://dprd.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/dprd.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>DPRD<BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://polrestamalangkota.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/polresta.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>POLRESTA<BR>MALANG KOTA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://www.instagram.com/kodim0833kotamalang/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kodim.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KODIM <BR>0833 </a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->


      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://www.kejari-malang.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kejari.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KEJARI <BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

       <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://pn-malang.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/pengadilan.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>PENGADILAN <BR> NEGERI</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://malangkota.bps.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bps.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BPS<BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://malangkota.bawaslu.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bawaslu.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BAWASLU <BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

       <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://kpud-malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/kpu.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>KPU <BR>KOTA MALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://tuguartha.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/bpr.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>BPR <BR>TUGU ARTHA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col --> 
 </div>
<h3  style=" font-family: Arial, Helvetica, sans-serif;">LAYANAN ADMINISTRASI PEMERINTAH</h3><br>
<div class="row">
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="400ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://ujikir.malangkota.go.id/" target="_blank">
                        <!-- <i class="tripo-icon-message"></i> -->
                        <img src="<?= base_url(); ?>assets/images/icon_new/dashboard.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                        <h3>DASHBOARD WALIKOTA</a></h3>
                     </div><!-- /.tour-category-one__single -->
                     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://malang.sipd.kemendagri.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/sipd.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIPD</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://efinance.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/efinance.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3> E-Finance</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://suradi.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/suradi.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SURADI</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://simpkk.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/simpkk.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIMPKK</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://simbada.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/simbada.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIMBADA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://simantau.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/e_tlhp.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>E-TLHP</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

        <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://simonita.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/simonita.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIMONITA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href=" https://play.google.com/store/apps/details?id=sipreti.malangkota.com&hl=es&gl=US" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/sipreti.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIPRETI</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://ekinerja.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/e-kinerja.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>E-KINERJA</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="http://simas.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/simas.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIMAS</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->
      <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                <div class="tour-category-one__single rounded-lg">
                    <a href="https://sipalang.malangkota.go.id/" target="_blank">
                        <img src="<?= base_url(); ?>assets/images/icon_new/sipalang.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                    <!-- <i class="tripo-icon-message"></i> -->
                    <h3>SIPALANG</a></h3>
                </div><!-- /.tour-category-one__single -->
     </div><!-- /.tour-category-one__col -->

                  
                    <div class="tour-category-one__col wow fadeInUp " data-wow-duration="1500ms" data-wow-delay="000ms">
                    <div class="tour-category-one__single rounded-lg">
                        <a href="https://redivet.malangkota.go.id/" target="_blank">
                            <img src="<?= base_url(); ?>assets/images/icon_new/redivet.png" style="height: 120px; width: 120px" class="main-logo"  width="" alt="Awesome Image" />
                        <!-- <i class="tripo-icon-message"></i> -->
                        <h3>REDIVET <br></a></h3>
                    </div><!-- /.tour-category-one__single -->
                    </div><!-- /.tour-category-one__col -->

</div> <!-- tutup row -->





 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
      <footer class="site-footer">
        <img src="<?= base_url(); ?>assets/images/wave3.png" style="width: 100%" class="banner-satu" width="" alt="Awesome Image" />
                      <!-- /.site-footer__bg -->
        <!-- /.site-footer__bg -->

                <div class="site-footer__bottom">
                      <div class="container">
                          <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
                          Pemerintah Kota Malang</a></p>
                          <div class="site-footer__social">
                            <a href="https://www.facebook.com/malangkota.go.id"><i class="fa fa-facebook-square"></i></a>
                            <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a>
                          </div><!-- /.site-footer__social -->
                      </div><!-- /.container -->
                  </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/malangkota.go.id"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

 <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>

</html>
