<?php
    date_default_timezone_set("Asia/Jakarta");

    $googleApiUrl = "http://api.openweathermap.org/data/2.5/weather?q=Malang,id&appid=2bbbfa2d8d8bdbf2e22701ff405ba7cc&lang=id&units=metric";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);

    curl_close($ch);
    $data = json_decode($response);
    $currentTime = time();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5am Cuaca</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">

    <!-- plugin scripts -->


    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:200,300,400,400i,500,600,700,800,900%7CSatisfy&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/tripo-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/vegas.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.pips.css">

    <!-- template styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/responsive.css">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }

    .card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 30%;
  border-radius: 5px;
}
    </style>
</head>

<body>
    <div class="preloader">
        <img src="<?= base_url(); ?>assets/images/loader.png" class="preloader__image" alt="">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <div class="site-header__header-one-wrap">

            <header class="main-nav__header-one ">
                <nav class="header-navigation stricky">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="main-nav__logo-box">
                            <a href="" class="main-nav__logo">
                                <img src="<?= base_url(); ?>assets/images/logo_navbar.png" class="main-logo" width="123" alt="Awesome Image" />
                            </a>
                            <a href="#" class="side-menu__toggler"><i class="fa fa-bars"></i>
                                <!-- /.smpl-icon-menu --></a>
                        </div><!-- /.logo-box -->
                        <!-- Collect the nav links, forms, and other content for toggling -->

                        <div class="main-nav__right">
                          <div class="searchbar" >
                            <input class="search_input"type="text" name="" placeholder="Search...">
                            <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
                          </div>
                        </div><!-- /.main-nav__right -->
                    </div>
                    <!-- /.container -->
                </nav>
            </header><!-- /.site-header -->
        </div><!-- /.site-header__header-one-wrap -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list">
    <div class="container"><br>
      <div class="tour-sorter-one">

           <div class="tour-sorter-one__right">

               <a href="<?= base_url(); ?>">Beranda</a>

           </div><!-- /.tour-sorter-one__right -->

       </div><!-- /.tour-sorter-one -->
      <center><h1 style=" font-family: Arial, Helvetica, sans-serif;">PERKIRAAN CUACA KOTA MALANG</h1><br><br>
        </center>

<h3  style=" font-family: Arial, Helvetica, sans-serif;"></h3><br>


    <div class="container card" >
       <!--  <h2><?php echo $data->name; ?> Weather Status</h2> -->
       <!--  <h2 style=" font-family: Arial, Helvetica, sans-serif; ">Kota Malang</h2> -->
            <h3 style=" font-family: Arial, Helvetica, sans-serif; "><?php echo date("l g:i a", $currentTime); ?></h3>
        <div class="time">
            <div></div>
            <div><?php echo date("jS F, Y",$currentTime); ?></div>
            <div><?php echo ucwords($data->weather[0]->description); ?></div>
        </div>
        <div class="weather-forecast">
            <img style="width: 100px"
                src="http://openweathermap.org/img/w/<?php echo $data->weather[0]->icon; ?>.png"
                class="weather-icon" /> <?php echo $data->main->temp_max; ?>°C<span
                class="min-temperature"><?php echo $data->main->temp_min; ?>°C</span>
        </div>
        <div class="time">
            <div>Clouds: <?php echo $data->clouds->all; ?> %</div>
            <div>Humidity: <?php echo $data->main->humidity; ?> %</div>
            <div>Pressure: <?php echo $data->main->pressure; ?> hPa</div>
            <div>Wind Direction: <?php echo $data->wind->deg; ?> °</div>
            <div>Wind Speed: <?php echo $data->wind->speed; ?> km/h</div>
        </div>
    </div>


 </div><!-- /.container -->

</section><!-- /.gallery-one -->

<!-- ================================================================================ -->
      <img src="<?= base_url(); ?>assets/images/iconic_mlg_nonbg.png" alt="Awesome Image" />

    <!-- /.site-footer__bg -->

    <div class="site-footer__bottom">
        <div class="container">
            <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
            Pemerintah Kota Malang</a></p>
            <div class="site-footer__social">
              <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i></a>
              <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i></a>
            </div><!-- /.site-footer__social -->
        </div><!-- /.container -->
    </div><!-- /.site-footer__bottom -->

</div><!-- /.page-wrapper -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->

 <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/typed-2.0.11.js"></script>
    <script src="<?= base_url(); ?>assets/js/vegas.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="<?= base_url(); ?>assets/js/theme.js"></script>
</body>

</html>
