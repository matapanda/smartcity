<?php
class Smartcity extends CI_Controller
{
  function __construct()
  {
      parent::__construct();
  }

  function index(){
      // $this->load->view('portal_malangkota_view');
    $this->load->view('smartcity_view/landingpage_smartcity_view');
  }

   function smart_governance(){
      $this->load->view('smartcity_view/menu_governance_view');
  }

   function smart_economy(){
      $this->load->view('smartcity_view/menu_economy_view');
  }
   function smart_branding(){
      $this->load->view('smartcity_view/menu_branding_view');
  }
  function smart_living(){
      $this->load->view('smartcity_view/menu_living_view');
  }
   function smart_society(){
      $this->load->view('smartcity_view/menu_society_view');
  }
  function smart_environment(){
      $this->load->view('smartcity_view/menu_environment_view');
  }
  function smart_health(){
      $this->load->view('smartcity_view/menu_health_view');
  }
  function cuaca(){
      $this->load->view('smartcity_view/menu_cuaca_view');
  }
  
   function webperangkatdaerah(){
      $this->load->view('smartcity_view/menu_web_perangkat_daerah_view');
  }
  function layananpublik(){
      $this->load->view('smartcity_view/menu_layanan_publik_view');
  }
  function tentang(){
      $this->load->view('smartcity_view/tentang_view');
  }
 function menu_puskesmas(){
      $this->load->view('smartcity_view/menu_puskesmas_view');
  }
 function menu_kec_klojen(){
      $this->load->view('smartcity_view/menu_kec_klojen_view');
  }
  function menu_kec_kedungkandang(){
      $this->load->view('smartcity_view/menu_kec_kedungkandang_view');
  }
  function menu_kec_lowokwaru(){
      $this->load->view('smartcity_view/menu_kec_lowokwaru_view');
  }
  function menu_kec_blimbing(){
      $this->load->view('smartcity_view/menu_kec_blimbing_view');
  }
  function menu_kec_sukun(){
      $this->load->view('smartcity_view/menu_kec_sukun_view');
  }
}
